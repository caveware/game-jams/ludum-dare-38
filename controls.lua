--[[
	Controls file
]]

return {
	accept = {'key:space', 'key:return', 'button:a'},
	down = {'key:down', 'key:s', 'axis:lefty+', 'button:dpdown'},
	left = {'key:left', 'key:a', 'axis:leftx-', 'button:dpleft'},
	right = {'key:right', 'key:d', 'axis:leftx+', 'button:dpright'},
	up = {'key:up', 'key:w', 'axis:lefty-', 'button:dpup'}
}