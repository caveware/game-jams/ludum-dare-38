--[[
	Load all graphics.
]]

return {
	characters = {
		chad = love.graphics.newImage(... .. '/characters/chad_base.png'),
		elijah = love.graphics.newImage(... .. '/characters/elijah_base.png'),
		shadow = {
			still = love.graphics.newImage(... .. '/characters/elijah_shadows.png'),
			moving = love.graphics.newImage(... .. '/characters/elijah_floats.png')
		},
		justine = love.graphics.newImage(... .. '/characters/girl1_base.png'),
		layla = {
			base = love.graphics.newImage(... .. '/characters/layla_base.png'),
			walk = {
				love.graphics.newImage(... .. '/characters/layla_walk_1.png'),
				love.graphics.newImage(... .. '/characters/layla_walk_2.png'),
				love.graphics.newImage(... .. '/characters/layla_walk_3.png'),
				love.graphics.newImage(... .. '/characters/layla_walk_4.png'),
				love.graphics.newImage(... .. '/characters/layla_walk_5.png'),
			}
		},
		marcia = love.graphics.newImage(... .. '/characters/girl4_base.png'),
		veronica = love.graphics.newImage(... .. '/characters/veronica_base.png'),
		weirdo = {
			still = love.graphics.newImage(... .. '/characters/weirdo_placeholder.png')
		},
		wendy = love.graphics.newImage(... .. '/characters/girl3_base.png')
	},
	decoration = {
		machine = love.graphics.newImage(... .. '/decoration/machine.png'),
		machine_done = love.graphics.newImage(... .. '/decoration/machine_done.png'),
		spinning_arrow = love.graphics.newImage(... .. '/decoration/spinning_arrow.png')
	},
	menu = {
		logo = love.graphics.newImage(... .. '/menu/logo.png')
	},
	overlay = {
		alley = {
			main = love.graphics.newImage(... .. '/overlays/alleylight.png'),
			map = love.graphics.newImage(... .. '/overlays/alleylight_light.png')
		}
	},
	text = {
		roboto = love.graphics.newFont(... .. '/text/Roboto-Bold.ttf')
	},
	ui = {
		action = love.graphics.newImage(... .. '/ui/action.png'),
		title = love.graphics.newImage(... .. '/ui/title.png')
	}
}