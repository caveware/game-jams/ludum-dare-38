--[[
	Load all assets for the game.
]]

gfx = require "assets/graphics"
maps = require "assets/maps"
snd = require "assets/sound"
cnv = require "assets/conversations"
fnt = require "assets/fonts"