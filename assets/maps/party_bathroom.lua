return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.14.2",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 13,
  height = 12,
  tilewidth = 24,
  tileheight = 24,
  nextobjectid = 16,
  properties = {},
  tilesets = {
    {
      name = "test",
      firstgid = 1,
      tilewidth = 24,
      tileheight = 24,
      spacing = 0,
      margin = 0,
      image = "../graphics/tilemaps/test.png",
      imagewidth = 48,
      imageheight = 24,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 2,
      tiles = {}
    },
    {
      name = "party_door",
      firstgid = 3,
      tilewidth = 24,
      tileheight = 24,
      spacing = 0,
      margin = 0,
      image = "../graphics/tilemaps/party_door.png",
      imagewidth = 48,
      imageheight = 96,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 8,
      tiles = {}
    },
    {
      name = "bathroom_background",
      firstgid = 11,
      tilewidth = 24,
      tileheight = 24,
      spacing = 0,
      margin = 0,
      image = "../graphics/tilemaps/bathroom_background.png",
      imagewidth = 72,
      imageheight = 72,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 9,
      tiles = {}
    },
    {
      name = "sally_base",
      firstgid = 20,
      tilewidth = 21,
      tileheight = 24,
      spacing = 0,
      margin = 0,
      image = "../graphics/characters/sally_base.png",
      imagewidth = 21,
      imageheight = 72,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 3,
      tiles = {}
    },
    {
      name = "girl3_base",
      firstgid = 23,
      tilewidth = 21,
      tileheight = 72,
      spacing = 0,
      margin = 0,
      image = "../graphics/characters/girl3_base.png",
      imagewidth = 21,
      imageheight = 72,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    },
    {
      name = "sink",
      firstgid = 24,
      tilewidth = 24,
      tileheight = 24,
      spacing = 0,
      margin = 0,
      image = "../graphics/unsorted/Sink.png",
      imagewidth = 24,
      imageheight = 24,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    },
    {
      name = "bath",
      firstgid = 25,
      tilewidth = 24,
      tileheight = 24,
      spacing = 0,
      margin = 0,
      image = "../graphics/unsorted/bath.png",
      imagewidth = 96,
      imageheight = 48,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 8,
      tiles = {}
    },
    {
      name = "window",
      firstgid = 33,
      tilewidth = 24,
      tileheight = 24,
      spacing = 0,
      margin = 0,
      image = "../graphics/unsorted/window.png",
      imagewidth = 48,
      imageheight = 24,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 2,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Tile Layer 1",
      x = 0,
      y = 0,
      width = 13,
      height = 12,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        11, 12, 13, 12, 13, 11, 11, 11, 11, 11, 11, 12, 13,
        11, 12, 11, 11, 11, 12, 13, 11, 12, 13, 13, 12, 13,
        11, 11, 12, 13, 12, 13, 11, 12, 13, 13, 13, 13, 13,
        11, 12, 13, 13, 13, 11, 11, 11, 11, 11, 11, 12, 13,
        14, 14, 15, 16, 14, 15, 16, 14, 15, 16, 14, 15, 16,
        17, 18, 19, 18, 19, 18, 19, 18, 19, 18, 19, 18, 19,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      name = "people",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      objects = {
        {
          id = 15,
          name = "wendy",
          type = "",
          shape = "rectangle",
          x = 216,
          y = 192,
          width = 21,
          height = 72,
          rotation = 0,
          gid = 23,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "tilelayer",
      name = "Decoration",
      x = 0,
      y = 0,
      width = 13,
      height = 12,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 33, 34, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 25, 26, 27, 28, 24, 0, 0, 0, 0,
        0, 0, 0, 0, 29, 30, 31, 32, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      name = "Tile Layer 2",
      x = 0,
      y = 0,
      width = 13,
      height = 12,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 5, 6, 0, 20, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 7, 8, 0, 21, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 9, 10, 0, 22, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      name = "spawnpoints",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      objects = {
        {
          id = 1,
          name = "upstairs",
          type = "",
          shape = "rectangle",
          x = 24,
          y = 120,
          width = 48,
          height = 72,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "actionpoints",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      objects = {
        {
          id = 8,
          name = "",
          type = "",
          shape = "rectangle",
          x = 24,
          y = 120,
          width = 48,
          height = 72,
          rotation = 0,
          visible = true,
          properties = {
            ["map_name"] = "party_upstairs",
            ["spawn_point"] = "bathroom"
          }
        }
      }
    },
    {
      type = "objectgroup",
      name = "collisions",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      objects = {
        {
          id = 9,
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 192,
          width = 312,
          height = 96,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 11,
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 0,
          width = 24,
          height = 192,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 12,
          name = "",
          type = "",
          shape = "rectangle",
          x = 288,
          y = 0,
          width = 24,
          height = 192,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
