--[[
	Load all maps
]]

return {
	-- Night / chase maps
	cafe = ... .. '/cafe.lua',
	home = ... .. '/home.lua',
	home_kitchen = ... .. '/home_kitchen.lua',
	night_alley = ... .. '/night_alley.lua',
	park = ... .. '/park.lua',

	-- Party maps
	party_hall = ... .. '/party_hall.lua',
	party_kitchen = ... .. '/party_kitchen.lua',
	party_living_room = ... .. '/party_living_room.lua',
	party_bathroom = ... .. '/party_bathroom.lua',
	party_outside = ... .. '/party_outside.lua',
	party_upstairs = ... .. '/party_upstairs.lua',

	-- Sewer maps
	sewer_1 = ... .. '/sewer_1.lua',
	sewer_2 = ... .. '/sewer_2.lua',
	sewer_3 = ... .. '/sewer_3.lua',
	sewer_final = ... .. '/sewer_final.lua'
}