return {
	{
		words = 'The next time I saw him, I was working at a cafe.'
	}, {
		event = function ()
			facing_right = false
		end,
		words = 'As I was about to close up for the night, he entered the store.'
	}, {
		words = 'I was surprised that he remembered me.'
	}, {
		words = 'After some talking, he ordered a coffee.'
	}, {
		words = 'And a pretentious one at that...'
	}
}