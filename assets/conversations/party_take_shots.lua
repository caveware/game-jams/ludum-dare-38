return {
	{
		name = 'Veronica',
		words = 'Want some shots, girl?',
		event = function ()
			if choices.taken_shots then
				Gamestate.current().conversation = Conversation({{
					words = 'Tequila...'
				}, {
					words = 'Vodka...'
				}, {
					words = 'Tequila again!'
				}, {
					name = 'Veronica',
					event = function ()
						Timer.during(2, function (dt)
							data.drunkeness = data.drunkeness + 0.01 * dt
						end)
					end,
					words = 'Whoa... You alright?'
				}})
			end
		end,
		choice = 'taken_shots'
	}
}