return {
	{
		name = 'Wendy',
		words = 'Yo Layla, what does this sink smell like?'
	}, {
		name = 'Layla',
		words = '*sniff* Uh, WHOA, cocaine??'
	}, {
		name = 'Wendy',
		event = function()
			choices.taken_coke = true
			Timer.during(2, function (dt)
				data.drunkeness = data.drunkeness + 0.01 * dt
			end)
		end,
		words = '10/10, correct answer.'
	}
}