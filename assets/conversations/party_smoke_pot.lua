return {
	{
		name = 'Craymork',
		words = 'Hey Layla, you smoke right?',
	}, {
		name = 'Craymork',
		words = 'Want some weed?',
		event = function ()
			if choices.smoked_pot then
				Gamestate.current().conversation = Conversation({{
					words = 'Puff...'
				}, {
					words = 'Puff...'
				}, {
					words = 'Pass...'
				}, {
					name = 'Craymork',
					event = function ()
						Timer.during(2, function (dt)
							data.drunkeness = data.drunkeness + 0.01 * dt
						end)
					end,
					words = 'Hahaha, you\'re crazy.'
				}})
			end
		end,
		choice = 'smoked_pot'
	}
}