--[[
	Load all the conversations
]]

return {
	intro = require(... .. '/intro'),

	-- Night stuff
	cat_transition = require(... .. '/cat_transition'),
	coffee_transition = require(... .. '/coffee_transition'),
	
	-- Party conversations
	elijah_fuck = require(... .. '/elijah_fuck'),
	elijah_hi = require(... .. '/elijah_hi'),
	justine = require(... .. '/justine'),
	marcia = require(... .. '/marcia'),
	party_intro = require(... .. '/party_intro'),
	party_locked_bedroom = require(... .. '/party_locked_bedroom'),
	party_no_acid = require(... .. '/party_no_acid'),
	party_smoke_pot = require(... .. '/party_smoke_pot'),
	party_smoked_pot = require(... .. '/party_smoked_pot'),
	party_take_acid = require(... .. '/party_take_acid'),
	party_take_coke = require(... .. '/party_take_coke'),
	party_take_shots = require(... .. '/party_take_shots'),
	party_taken_coke = require(... .. '/party_taken_coke'),
	party_taken_shots = require(... .. '/party_taken_shots'),
	party_transition = require(... .. '/party_transition')
}