--[[
	Loads all game fonts
]]

return {
	default = love.graphics.newFont(... .. '/perfectdos16.ttf', 16)
}