--[[
	Loads all sounds
]]

local snd = {
	bgm = {
		cafe = love.audio.newSource(... .. '/bgm/cafe.ogg', 'stream'),
		campfire = love.audio.newSource(... .. '/bgm/campfire.ogg', 'stream'),
		chase = love.audio.newSource(... .. '/bgm/chase.ogg', 'stream'),
		home = love.audio.newSource(... .. '/bgm/home.ogg', 'stream'),
		park = love.audio.newSource(... .. '/bgm/park.ogg', 'stream'),
		party = love.audio.newSource(... .. '/bgm/party.ogg', 'stream'),
		sewer_1 = love.audio.newSource(... .. '/bgm/sewer_1.ogg', 'stream'),
		sewer_2 = love.audio.newSource(... .. '/bgm/sewer_2.ogg', 'stream'),
		sewer_3 = love.audio.newSource(... .. '/bgm/sewer_3.ogg', 'stream'),
	},
	dwyer_fucked = love.audio.newSource(... .. '/dwyer_fucked.ogg', 'static'),
	menu = {
		coffee = {
			arrow = love.audio.newSource(... .. '/menu/coffee_arrow.ogg', 'static'),
			complete = love.audio.newSource(... .. '/menu/coffee_complete.ogg', 'static'),
			settle = love.audio.newSource(... .. '/menu/coffee_settle.ogg', 'static')
		},
		convo = love.audio.newSource(... .. '/menu/convo.ogg', 'static'),
		ded = love.audio.newSource(... .. '/menu/ded.ogg', 'static'),
		door = love.audio.newSource(... .. '/menu/door.ogg', 'static'),
		jump = love.audio.newSource(... .. '/menu/jump.ogg', 'static'),
		startup = love.audio.newSource(... .. '/menu/startup.ogg', 'static'),
		title = love.audio.newSource(... .. '/menu/title.ogg', 'static')
	}
}

for key, bgm in ipairs(snd.bgm) do
	bgm:setLooping(true)
end

return snd