--[[
	NPCs
]]

local NPC = Class({
	x = 0,
	y = 0
})

function NPC:init(x, y, image)
	self.x = x or self.x
	self.y = y or self.y
	self.image = image

	self.width = image:getWidth()
	self.height = image:getHeight()
end

function NPC:update(player_x)
	self.facing_right = player_x > self.x
end

function NPC:draw()
	local scale = self.facing_right and 1 or -1
	local w = self.width / 2
	love.graphics.draw(self.image, self.x + w, self.y, 0, scale, 1, w, 0)
end

return NPC