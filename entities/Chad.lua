local Chad = Class({
	__includes = NPC
})

function Chad:talkTo()
	return Conversation(choices.smoked_pot and cnv.party_smoked_pot or cnv.party_smoke_pot)
end

return Chad