--[[
	Quicktime sequence for the barrista minigame
	Will hard code in for barrista, but would
	be easy to modify for any quicktime sequence.
]]

--Lua table for the sequence
--Will include the sequence itself as well as 
--Outcome function


--Lua table for sequence outcome

--Class creation
local Quicktime = Class({
	place = 0, --will be incremented to 1 on init.
	active_timer = true, --true because it'll give weird results if I mess up.'
	should_draw = false
})

Quicktime.sequence = {
	{
		text = "LEFT NOW",
		input = 'left',
		wait = 1,
		opportunity = 5
	},
	{
		text = "RIGHT NOW",
		input = 'right',
		wait = 1,
		opportunity = 4
	},
	mistakes = 0,
	max_mistakes = 3
}

--Init constructor
function Quicktime:init()
	--call the first timer
	self.active_quicktime = self
	self.timer = 0
	self:next()
end

--Draw

function Quicktime:draw()
	if self.should_draw then
		love.graphics.print(
			self.sequence[self.place].text,
			100,
			100
		)
	end

		love.graphics.print(self.timer,
		100,
		120)

	love.graphics.print(self.sequence.mistakes,
		100,
		130
	)
	love.graphics.draw(gfx.decoration.barrista_machine,GAME_WIDTH/2*SCALE,GAME_HEIGHT/2*SCALE,0,SCALE,SCALE,192/2,192/2)
end
--Update
--for input handling / maybe. I dunno. Maybe
--will use functions to trigger the timer stuff separately.
--likely will use timers to handle all the state changes.
function Quicktime:update(dt)
	self.timer = self.timer + dt
	local complete = false
	local wait = self.sequence[self.place].wait
	local opportunity = self.sequence[self.place].opportunity

	--Check if waiting
	if self.timer > wait then
		self.should_draw = true
		--check opportunity
		complete = self:handleInput(self.sequence[self.place].input)
		if complete then
			--right on time
			--Handle success
			self:next()
		end
		--check if too late
		if self.timer > wait + opportunity then
			--handle failure
			self:failure()
			self:next()
		end
	else
		--check if too early
		complete = self:handleInput(self.sequence[self.place].input)
		if complete then
			--too early
			--Handle failure
			self:failure()
			self:next()
		end
	end
end

--Next
--determine success or failure
function Quicktime:next()
	self.should_draw = false
	self.timer = 0
	--go to the next step in the sequence.
	if #self.sequence > self.place then
		self.place = self.place + 1
	else
		self:finish()
	end
	
end

--Finish
function Quicktime:finish()
	if self.sequence.mistakes <= self.sequence.max_mistakes then
		choices['barrista'] = true
	else
		choices['barrista'] = false
	end

	Gamestate.switch(state.play)
end

--Success
--special is a function to let you do fancy stuff.
function Quicktime:success(special)
	if special then
		special()
	end
end

--Failure
function Quicktime:failure(special)
	if special then
		special()
	end
	self.sequence.mistakes = self.sequence.mistakes + 1
end

--deal with user input based on the current state.
--maybe.
function Quicktime:handleInput(input)
	--called from the timers.
	--returns whether or not the player pressed
	--the correct key in the correct time.
	if Input:pressed(input) then
		return true
	end
	return false
end


return Quicktime