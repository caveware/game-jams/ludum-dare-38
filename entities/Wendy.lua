local Wendy = Class({
	__includes = NPC
})

function Wendy:talkTo()
	return Conversation(choices.taken_coke and cnv.party_taken_coke or cnv.party_take_coke)
end

return Wendy