--[[
	Load all entities.
]]

Barista = require(... .. '/Barista')
Player = require(... .. '/Player')
Conversation = require(... .. '/Conversation')
NPC = require(... .. '/NPC')
Quicktime = require(... .. '/Quicktime')
SM = require(... .. '/SoundManager')(.25, 1)
Weirdo = require(... .. '/Weirdo')

npcs = {
	chad = require(... .. '/Chad'),
	elijah = require(... .. '/Elijah'),
	justine = require(... .. '/Justine'),
	marcia = require(... .. '/Marcia'),
	veronica = require(... .. '/Veronica'),
	wendy = require(... .. '/Wendy')
}