--[[
	Weirdo who chases you through the alley
]]

local Weirdo = Class({
	facing_right = false,
	running = false,
	width = 21,
	height = 72
})

function Weirdo:init(x, y, running)
	self.running = self.running == nil and running or self.running
	self.x = x
	self.y = y
end

function Weirdo:update(dt, collisions)
	self.running = true
	self.x = math.min(self.x + dt * 120, 4434)

	local selfExtended = {
		x = self.x,
		y = self.y,
		width = 128,
		height = 72
	}

	local col = Hit.checkRectangleTable(self, collisions)

	if col then
		self.y = math.max(self.y - 60 * dt, col.y - 72)
	else
		self.y = self.y + dt * 60
		local col = Hit.checkRectangleTable(self, collisions)
		if col then
			self.y = col.y - 72
		end
	end
end

function Weirdo:draw()
	local image = self.running and gfx.characters.shadow.moving or gfx.characters.shadow.still
	love.graphics.draw(image, self.x + 10, self.y + 36, (math.random() - 0.5) * 0.05, 1, 1, 10, 36)
end

return Weirdo