--[[
	Barista minigame
]]

local Barista = Class({
	x = 0,
	y = 0
})

function Barista:init(x, y, on_quit)
	self.x = x or self.x
	self.y = y or self.y
	self.on_quit = on_quit

	self.angle = 0
	self.alpha = 0
	self.count = 0
	self.points = 0
	self.timer = 5

	self:setTimer()

	Timer.tween(.5, self, {
		alpha = 1
	}, 'in-out-quad')
end

function Barista:update(dt)
	if self.still and self.alpha == 1 then
		self.timer = self.timer - dt

		if Input:pressed(self:getAngle()) then
			self.count = self.count + 1
			self.points = self.points + self.timer
			self:setTimer()
		elseif self.timer <= 0 then
			self.count = self.count + 1
			self:setTimer()
		end
	end
end

function Barista:draw()
	local image = (self.count == 10) and gfx.decoration.machine_done or gfx.decoration.machine
	love.graphics.draw(image, x, y)

	love.graphics.draw(gfx.decoration.spinning_arrow, self.x + 240, self.y + 94, self.angle, 1, 1, 50, 50)

	love.graphics.setColor(0, 0, 0, 255 - (self.alpha * 255))
	love.graphics.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)

	love.graphics.setColor(255, 255, 255)
end


function Barista:getAngle()
	local val = math.floor(self.angle % (math.pi * 2))

	if val == 1 then
		return 'right'
	elseif val == 3 then
		return 'down'
	elseif val == 4 then
		return 'left'
	else
		return 'up'
	end
end

function Barista:quit()
	Timer.tween(2, self, {
		alpha = 0
	}, 'in-out-quad', function ()
		self.on_quit(self.points)
	end)
end

function Barista:setTimer()
	if self.count == 10 then
		snd.menu.coffee.complete:play()
		self:quit()
		return
	end

	snd.menu.coffee.arrow:setVolume(1/3)
	snd.menu.coffee.arrow:play()
	self.still = false

	Timer.tween(1, self, {
		angle = self.angle + math.random(14, 17) / 2 * math.pi
	}, 'in-out-quad', function ()
		self.still = true
		self.timer = 2
		snd.menu.coffee.settle:setVolume(1/3)
		snd.menu.coffee.settle:play()
	end)
end


return Barista
