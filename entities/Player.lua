--[[
	Player object
]]

local Player = Class({
	x = 0,
	y = 0,
	width = 20,
	height = 72,
	can_jump = false,
	facing_right = true,
	image = gfx.characters.layla,
	is_frozen = false,
	moving = false,
	step = 0,
	y_speed = 0
})

function Player:init(x, y)
	self.x = x
	self.y = y

	self.walk = {1, 2, 3, 4}

	self:setFrozen(false)
end

function Player:draw()
	local x_scale = self.facing_right and 1 or -1
	local image = self.image[self.moving and 'walk' or 'base']
	if self.moving then
		image = image[self.walk[math.floor(self.step + 1)]]
	end
	local w = image:getWidth() / 2

	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(image, self.x + w, self.y, 0, x_scale, 1, w, 0)

	if (self.action_available) then
		love.graphics.draw(gfx.ui.action, self.x + 10, self.y - 10, math.sin(love.timer.getTime() * 4) * 0.8, 1, 1, 6, 6)
	end
end

function Player:update(dt, collisions)
	if not self.is_frozen then
		local x_move = Input:get('right') - Input:get('left')

		-- Horizontal movement
		self.moving = x_move ~= 0
		self.x = self.x + 120 * dt * x_move
		if self.y_speed ~= 0 then
			self.step = 0
		elseif self.moving then
			self.step = (self.step + dt * 6) % #self.walk
		end

		-- Horizontal collision detection
		local x_collision = Hit.checkRectangleTable(self, collisions)
		if x_collision then
			local x_up = Hit.checkRectangleTable({
				x = self.x,
				y = self.y - 8,
				width = self.width,
				height = self.height
			}, collisions)

			if not x_up then
				self.y = self.y - 8
			else
				self.x = x_move > 0
					and x_collision.x - self.width
					or x_collision.x + x_collision.width
			end
		end

		-- Change facing
		if x_move > 0 then
			self.facing_right = true
		elseif x_move < 0 then
			self.facing_right = false
		end

		-- Gravity
		self.y_speed = self.y_speed + GRAVITY * dt * 2
		self.y = self.y + self.y_speed * dt

		-- Vertical collision detection
		local y_collision = Hit.checkRectangleTable(self, collisions)
		if y_collision then
			self.can_jump = true
			self.y_speed = 0
			self.y = y_collision.y - self.height
		end

		-- Jumping
		if Input:pressed('up') and self.can_jump then
			self.can_jump = false
			self.y_speed = -250
			snd.menu.jump:play()
		end
	end
end

function Player:setActionAvailable(available)
	self.action_available = available
end

function Player:setFrozen(is_frozen)
	self.is_frozen = is_frozen
end

return Player
