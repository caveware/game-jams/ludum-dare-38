--[[
	BGM manager
]]

local SM = Class({
	fade_speed = .5,
	max_volume = 1
})

function SM:init(fade_speed, max_volume)
	self.fade_speed = fade_speed or self.fade_speed
	self.max_volume = max_volume or self.max_volume
end

function SM:play(bgm)
	if bgm == self.current_sound then
		return
	end

	-- Stop previous sound
	if self.previous_sound then
		self.previous_sound:stop()
		Timer.cancel(self.previous_sound_timer)
	end

	if self.current_sound then
		Timer.cancel(self.current_sound_timer)
		self.current_sound:setVolume(self.max_volume)

		self.previous_sound = self.current_sound
		self.previous_sound_timer = Timer.during(self.fade_speed, function (dt)
			self.previous_sound:setVolume(self.previous_sound:getVolume() - dt / self.fade_speed * self.max_volume)
		end, function ()
			self.previous_sound:stop()
			self.previous_sound = nil
		end)
	end

	if not bgm then
		self.current_sound = nil
		return
	end

	bgm:setLooping(true)
	bgm:setVolume(0)
	bgm:play()
	self.current_sound = bgm
	self.current_sound_timer = Timer.during(self.fade_speed, function (dt)
		bgm:setVolume(bgm:getVolume() + dt / self.fade_speed * self.max_volume)
	end, function ()
		bgm:setVolume(self.max_volume)
	end)
end

return SM