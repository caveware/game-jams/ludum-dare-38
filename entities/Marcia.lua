local Marcia = Class({
	__includes = NPC
})

function Marcia:talkTo()
	return Conversation(cnv.marcia)
end

return Marcia