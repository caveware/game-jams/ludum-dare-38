local Veronica = Class({
	__includes = NPC
})

function Veronica:talkTo()
	return Conversation(choices.taken_shots and cnv.party_taken_shots or cnv.party_take_shots)
end

return Veronica