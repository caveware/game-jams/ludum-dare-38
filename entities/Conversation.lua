--[[
	Conversation Object

	Steps through a conversation table displaying:
	who
	what they say
	
	and can trigger an event (a function).
]]

local bar_height = 0.2 --10% of screen height

-- The basic information required
local Conversation = Class({
	font = fnt.default,
	place = 1, --where in the convo are we?
	bar_1 = {
		x1 = 0,
		y1 = 0,
		x2 = GAME_WIDTH * SCALE,
		y2 = GAME_HEIGHT * SCALE * bar_height
	},
	bar_2 = {
		x1 = 0,
		y1 = GAME_HEIGHT * SCALE * (1 - bar_height),
		x2 = GAME_WIDTH * SCALE,
		y2 = GAME_HEIGHT * SCALE
	},
	waited = 0,-- will wait for 0.3 seconds to prevent accidentally missing conversation
	wait_time = 0.3, --default
	selection = true, --for choice
	text_x = GAME_WIDTH * 0.25 * SCALE,
	text_y = (GAME_HEIGHT * .8) * SCALE
})

-- Load and start the required conversation
function Conversation:init(conversation)
	self.conversation = conversation
	self:tweenBarsIn()
end

function Conversation:update(dt)
	if self.waited > self.wait_time then
		--check for player input
		--advance conversation
		if Input:pressed('accept') then
			--Handle Choice
			if self.conversation[self.place].choice then
				self:handleChoice()
			end
			self:advanceConversation()
			self.waited = 0
		end
	end
	--update selection
	if self.conversation[self.place].choice then
		if Input:pressed('left') or Input:pressed('up') or Input:pressed('right') or Input:pressed('down') then
			self.selection = not self.selection
		end
	end
	
	self.waited = self.waited + dt
end

-- Draw the current conversation on screen
function Conversation:draw()
	--Background Bars
	--Actually do them in the conversation itself.

	love.graphics.setColor(0,0,0)
	love.graphics.rectangle('fill',
		self.bar_1.x1,self.bar_1.y1,
		self.bar_1.x2,self.bar_1.y2
		)
	love.graphics.rectangle('fill',
		self.bar_2.x1,self.bar_2.y1,
		self.bar_2.x2,self.bar_2.y2)
	love.graphics.setFont(self.font)
	love.graphics.setColor(255,255,255)
	local name = self.conversation[self.place].name
	if name and name ~= '' then
		love.graphics.print(name .. ": ",self.text_x,self.text_y)
	end
	love.graphics.printf(self.conversation[self.place].words, self.text_x, self.text_y + 13,GAME_WIDTH * SCALE * .5)

	--Current Selection
	if self.conversation[self.place].choice then
		love.graphics.setColor(0,255,0)
		love.graphics.print("YES",self.text_x + 16,self.text_y + 26)
		love.graphics.setColor(255,0,0)
		love.graphics.print("NO",self.text_x + 64,self.text_y + 26)

		love.graphics.setColor(255, 255, 255)
		love.graphics.circle('fill', self.text_x + (self.selection and 8 or 56), self.text_y + 34, 4, 4)
	end
end

function Conversation:tweenBarsIn()
	--Todo: Make it tween in the black bars and fade the text
	self.bar_1 = {
		x1 = 0,
		y1 = 0,
		x2 = GAME_WIDTH * SCALE,
		y2 = 0--GAME_HEIGHT * SCALE * bar_height
	}

	self.bar_2 = {
		x1 = 0,
		y1 = GAME_HEIGHT * SCALE,--GAME_HEIGHT * SCALE * (1 - bar_height),
		x2 = GAME_WIDTH * SCALE,
		y2 = GAME_HEIGHT * SCALE
	}
	Timer.tween(
		.2,
		self.bar_1,
		{
			x1 = 0,
			y1 = 0,
			x2 = GAME_WIDTH * SCALE,
			y2 = GAME_HEIGHT * SCALE * bar_height
		}
	)

	Timer.tween(
		.2,
		self.bar_2,
		{
			x1 = 0,
			y1 = GAME_HEIGHT * SCALE * (1 - bar_height),
			x2 = GAME_WIDTH * SCALE,
			y2 = GAME_HEIGHT * SCALE
		}
	)
end

function Conversation:advanceConversation()
	snd.menu.convo:play()

	--advance conversation, trigger event.
	if self.conversation[self.place].event then
		self.conversation[self.place].event()
	end

	if #self.conversation > self.place then
		self.place = self.place + 1
	else
		self.complete = true
	end
end

function Conversation:handleChoice()
	choices[self.conversation[self.place].choice] = self.selection
end

return Conversation