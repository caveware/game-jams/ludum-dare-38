local Justine = Class({
	__includes = NPC
})

function Justine:talkTo()
	return Conversation(cnv.justine)
end

return Justine