--[[
	Game libraries
]]

-- HUMP libraries
Camera = require 'libs.hump.camera'
Class = require 'libs.hump.class'
Gamestate = require 'libs.hump.gamestate'
Signal = require 'libs.hump.signal'
Timer = require 'libs.hump.timer'

-- Caveware Digital libraries
require 'libs.caveware'

-- Other libraries
_ = require 'libs.moses'
Baton = require 'libs.baton'
Lovebird = require 'libs.lovebird'
STI = require 'libs.sti'
