-- Handles checking AABB collisions

local Hit = {}

function Hit.checkRectangles(rect1, rect2)
	return
		rect1.x < rect2.x + rect2.width and
		rect1.y < rect2.y + rect2.height and
		rect2.x < rect1.x + rect1.width and
		rect2.y < rect1.y + rect1.height
end

function Hit.checkRectangleTable(rect, rectTable)
	local found = nil
	for z, rectA in ipairs(rectTable) do
		if Hit.checkRectangles(rect, rectA) then
			found = rectA
		end
	end
	return found
end

return Hit