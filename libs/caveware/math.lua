-- Extra maths functions

--[[
	Restricts the value to be inside of the given range
]]
function math.clamp(min, value, max)
	return math.min(max, math.max(value, min))
end

--[[
	Rounds the value given
]]
function math.round(value)
	return math.floor(value + .5)
end

--[[
	Returns the sign of a given value
]]
function math.sign(value)
	return value > 0 and 1 or (value < 0 and -1 or 0)
end
