--[[
	Configuration file for Ludum Dare 38.
]]

function love.conf(game)
game.console = true
	game.window.width = 960
	game.window.height = 272 * 2
	game.window.resizable = false
	game.window.title = 'FANCY SEEING YOU HERE'

game.releases = {
	name = "fsyh",
	title = 'fsyh',
	package = 'fsyh',
	loveVersion = '0.10.2',
	version = nil,
	author = 'Josef Frank',
	email = 'josef@caveware.digital',
	description = nil,
	homepage = 'https://caveware.digital',
	identifier = 'fsyh',
	excludeFileList = {''},
	compile = false,
	releaseDirectory = 'releases',
}
end