--[[
	State for the credits
]]

local Credits = {}

function Credits:enter(previous_state,on_quit)
	self.s = snd.dwyer_fucked
	self.s:play()
end

function Credits:update(dt)
	if not self.s:isPlaying() then
		love.event.quit()
	end
end

function Credits:draw()
end

return Credits