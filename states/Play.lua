--[[
	Play state
]]

local Play = {}

Play.drunk_timer = {nil,nil}

function Play:init()
	Signal.register('cat_transition', function ()
		if data.left_house then
			self.conversation = Conversation({
				{
					words = 'You don\'t want to look at it.'
				}
			})
		else
			self:changeRoom({
				properties = {
					state_name = 'cat_transition'
				}
			})
			data.left_house = true
			snd.menu.door:play()
		end
	end)

	Signal.register('coffee_transition', function ()
		if data.coffee then
			self.conversation = Conversation({
				{
					words = 'You don\'t want to look at that.'
				}
			})
		else
			self:changeRoom({
				properties = {
					state_name = 'coffee_transition'
				}
			})
			snd.menu.door:play()
		end
	end)

	Signal.register('party_transition', function ()
		if data.fucked_elijah then
			self.conversation = Conversation({
				{
					words = 'You don\'t want to look at them.'
				}
			})
		else
			self:changeRoom({
				properties = {
					state_name = 'party_transition'
				}
			})
			snd.menu.door:play()
		end
	end)

	Signal.register('to_sewer_2', function ()
		if data.fucked_elijah then
			self:changeRoom({
				properties = {
					map_name = 'sewer_2'
				}
			})
			snd.menu.door:play()
		else
			self.conversation = Conversation({
				{
					words = 'The door is locked.'
				}
			})
		end
	end)

	Signal.register('to_sewer_3', function ()
		if data.coffee then
			self:changeRoom({
				properties = {
					map_name = 'sewer_3'
				}
			})
			snd.menu.door:play()
		else
			self.conversation = Conversation({
				{
					words = 'The door is locked.'
				}
			})
		end
	end)

	Signal.register('to_sewer_final', function ()
		if data.left_house then
			self:changeRoom({
				properties = {
					map_name = 'sewer_final'
				}
			})
			snd.menu.door:play()
		else
			self.conversation = Conversation({
				{
					words = 'The door is locked.'
				}
			})
		end
	end)
end

function Play:enter(previous_state, map_name, spawn_point)
	self.map_name = map_name
	self.spawn_point = spawn_point

	-- Determine map from given map name or test
	self.map = STI(maps[map_name or 'test'])

	local mn = (map_name == 'home_kitchen' or (data.cat and map_name == 'sewer_3') or map_name == 'sewer_final') and 'home' or map_name

	if snd.bgm[mn] then
		SM:play(snd.bgm[mn])
	elseif map_name == 'night_alley' then
	elseif map_name == 'party_outside' then
		SM:play(snd.bgm.campfire)
	else
		SM:play(snd.bgm.party)
	end

	-- Locate spawn point
	local spawn = _.findWhere(self.map.layers['spawnpoints'].objects, {
		name = spawn_point or 'initial'
	})

	-- Spawn player at given point and remove layer
	self.player = Player(spawn.x + 8, spawn.y)
	self.map:removeLayer('spawnpoints')
	if facing_right ~= nil then
		self.player.facing_right = facing_right
	end

	-- Deal with 'Actions' layer
	self.actions = self.map.layers['actionpoints'].objects
	self.map:removeLayer('actionpoints')

	self.collisions = self.map.layers['collisions'].objects
	self.map:removeLayer('collisions')

	local light_layer = self.map.layers['lights']
	self.lights = light_layer and light_layer.objects or nil

	local char_layer = self.map.layers['people']
	if char_layer then
		self.chars = _.map(char_layer.objects, function (k, v)
			return npcs[v.name](v.x, v.y - v.height, gfx.characters[v.name])
		end)
		self.map:removeLayer('people')
	else
		self.chars = {}
	end

	self.camera = Camera(self.player.x + (self.player.facing_right and 110 or -90), GAME_HEIGHT / 2, SCALE)
	self.camera.smoother = Camera.smooth.damped(8)

	--Conversation, nil or active.
	self.conversation = nil

	if map_name == 'party_hall' and not data.had_hall_dialogue then
		self.conversation = Conversation(cnv.party_intro)
	elseif map_name == 'cafe' then
		if not data.made_coffee then
			self.conversation = Conversation({{
				name = 'Elijah',
				words = 'I\'ll have a 13 shot venti soy hazelnut vanilla cinnamon white mocha with extra white mocha and caramel.'
			}, {
				name = 'Layla',
				words = '...'
			}, {
				name = 'Layla',
				event = function ()
					Gamestate.switch(state.minigame, function ()
						Gamestate.switch(state.play, 'cafe')
					end)
				end,
				words = 'Fuck you, dude.'
			}})
			data.made_coffee = true
		else
			self.conversation = Conversation({
				{
					name = 'Layla',
					words = 'Here\'s your stupid shitty Starbucks coffee.'
				}, {
					name = 'Elijah',
					words = 'Thanks. So, could I get your number?'
				}, {
					name = 'Layla',
					words = 'Okay, dude, the only reason I slept with you was because I was drunk.'
				}, {
					name = 'Layla',
					words = 'I have no interest in seeing you ever again.'
				}, {
					name = 'Layla',
					words = 'So no.'
				}, {
					name = 'Elijah',
					event = function ()
						Gamestate.switch(state.blank_conversation, {{
							words = 'That was creepy, but things only got creepier.'
						}, {
							words = 'After that coffee, you locked up for the night.'
						}}, function ()
							Gamestate.switch(state.play, 'night_alley', 'cafe')
							SM:play()
							Gamestate.current().conversation = Conversation({
								{
									words = 'You notice a creepy man in the alley.'
								}, {
									words = 'Rather, he makes himself known...'
								}, {
									name = '???',
									words = 'Finally... I\'ve been waiting for you.'
								}, {
									event = function ()
										SM:play(snd.bgm.chase)
									end,
									name = '???',
									words = 'COME HERE, CUTIE!'
								}
							})
						end)
					end,
					words = 'That\'s okay, I don\'t need it anyway.'
				}
			})
		end
	elseif map_name == 'park' then
		self.conversation = Conversation({{
			name = 'Layla',
			words = 'Who the FUCK was that???'
		}, {
			name = 'Layla',
			words = 'I need to get home, and quick...'
		}})
	elseif map_name == 'home' then
		self.conversation = Conversation({{
			name = 'Layla',
			words = 'Oh my god...'
		}})
	elseif map_name == 'sewer_final' then
		self.conversation = Conversation({{
			name = 'Layla',
			words = '... He\'s dead.'
		}, {
			name = 'Layla',
			words = 'God damn...'
		}})
	end

	--For fade ins and fade outs
	self.alpha = {255}
	--fadein
	self.transition = true
	self.player:setFrozen(true)
	Timer.tween(
		0.25,
		self.alpha,
		{0},
		'linear',
		function()
			self.transition = false
			self.player:setFrozen(false)
		end
	)

	local map_height = self.map.height * self.map.tileheight + GAME_HEIGHT * 4
	local map_width = self.map.width * self.map.tilewidth + GAME_WIDTH * 4

	self.canvas = love.graphics.newCanvas(map_width, map_height)

	self.light = love.graphics.newImage('assets/graphics/overlays/fire.png')
	self.light_map = love.graphics.newImage('assets/graphics/overlays/fire_light.png')

	if map_name == 'night_alley' then
		self.weirdo = Weirdo(120, 120)
	else
		self.weirdo = nil
	end
end

function Play:update(dt)
	if not self.transition then
		if self.conversation then

			if self.conversation.complete then
				self.conversation = nil
			else
				self.conversation:update(dt)
			end
		else
			self.player:update(dt, self.collisions)
			facing_right = self.player.facing_right

			local action = Hit.checkRectangleTable(self.player, self.actions)
			if not action then
				action = Hit.checkRectangleTable(self.player, self.chars or {})
			end
			self.player:setActionAvailable(not not action)

			self:checkActions(action)

			if self.weirdo then
				self.weirdo:update(dt, self.collisions)

				if self.player.x - self.weirdo.x < 18 then
					snd.menu.ded:play()
					Gamestate.switch(state.play, self.map_name, self.spawn_point)
				end
			end

			if self.map_name == 'park' and self.player.x > 1032 then
				self.conversation = Conversation({{
					name = 'Elijah',
					words = 'Fancy seeing you here.'
				}, {
					name = 'Layla',
					words = 'Dude... What\'s going on?'
				}, {
					name = 'Elijah',
					words = 'I just like parks.'
				}, {
					name = 'Elijah',
					words = 'After dark.'
				}, {
					name = 'Elijah',
					words = 'And the people you can see...'
				}, {
					name = 'Layla',
					words = 'Ah... haha... I\'ll just be going home now...'
				}, {
					name = 'Elijah',
					words = 'See you soon...'
				}, {
					event = function ()
						data.coffee = true
						Gamestate.switch(state.play, 'sewer_2', 'coffee')
					end,
					words = 'Seeing him ever again would be too soon.'
				}})
			end

			if self.map_name == 'home' and self.player.x > 224 and not AAAH then
				self.conversation = Conversation({
					{
						words ='*ring ring*'
					}, {
						words = 'You pick up the phone.'
					},
					{
					name = 'Elijah',
					words = 'Nice place... If you want to see your cat again, come to the sewer behind your place.'
				}, {
					words = '*click*'
				}})
				AAAH = true
			end

			if self.map_name == 'sewer_final' and self.player.x > 336 and not ded then
				self.conversation = Conversation({
					{
						words ='There is a letter here from Elijah.'
					}, {
						name = 'Elijah',
						words = "I've watched you ever since the party."
					}, {
						name = 'Elijah',
						words = "I never had any luck with the girls."
					}, {
						name = 'Elijah',
						words = "You're all the same. Ignoring me even though I'm GREAT."
					}, {
						name = 'Elijah',
						words = "Wasn't I handsome? Wasn't I everything you wanted?"
					}, {
						name = 'Elijah',
						words = "No, I was a moment."
					}, {
						name = 'Elijah',
						words = "A drunken, regrettable moment."
					}, {
						name = 'Elijah',
						words = "But now I'll be with you forever."
					}, {
						name = 'Elijah',
						words = "Haunting your dreams, scrambling your brain."
					}, {
						name = 'Elijah',
						words = "You'll go crazy over me."
					}, {
						name = 'Elijah',
						event = function ()
							SM:play()
						end,
						words = "I don't care if I was what you wanted."
					}, {
						name = 'Elijah',
						words = "Because now I'm everything."
					}, {
						words = "..."
					}, {
						words = "FANCY SEEING YOU HERE"
					}, {
						words = "A game by Caveware Digital"
					}, {
						name = 'TEAM:',
						event = function ()
							Gamestate.switch(state.credits)
						end,
						words = "JOSEF FRANK / KEIRAN DAWSON / MATHEW DWYER / ROBERT PRESTON"
					}})
				ded = true
			end
		end

		for k, v in ipairs(self.chars or {}) do
			v:update(self.player.x)
		end
	end

	local t = math.sin((love.timer.getTime() % 10) / 10 * math.pi * 2)

	self.camera:rotateTo(data.drunkeness * t)

	if self.map_name == 'party_hall' and not data.had_hall_dialogue and self.conversation and self.conversation.complete then
		data.had_hall_dialogue = true
		self.conversation = Conversation(choices['take_acid'] and cnv.party_take_acid or cnv.party_no_acid)
	end

	self:updateCamera(dt)
end

function Play:draw()
	local show_overlay = false
	if self.map_name == 'party_outside' then
		self:drawPartyOutsideOverlay()
		show_overlay = true
	elseif self.lights then
		self:drawAlleyOverlay()
		show_overlay = true
	end

	love.graphics.setColor(255, 255, 255, 255)
	self.camera:attach()
		self.map:draw()

		for k, v in ipairs(self.chars or {}) do
			v:draw()
		end

		self.player:draw()

		if self.weirdo then
			self.weirdo:draw()
		end

		if show_overlay then
			love.graphics.setColor(255, 255, 255, 210)
			love.graphics.draw(self.canvas, -GAME_WIDTH * 2, -GAME_HEIGHT * 2)
		end
	self.camera:detach()

	if self.conversation then
		self.conversation:draw()
	end

	if self.transition then
		love.graphics.setColor(0,0,0,self.alpha[1])
		love.graphics.rectangle("fill",0,0,GAME_WIDTH * SCALE,GAME_HEIGHT * SCALE)
	end
end


function Play:updateCamera(dt)
	local map_height = self.map.tileheight * self.map.height
	local map_width = self.map.tilewidth * self.map.width

	local height = GAME_HEIGHT / 2
	local width = GAME_WIDTH / 2

	local x = self.player.x + self.player.width / 2
	x = x + (self.player.facing_right and 100 or -100)

	if self.weirdo then
		x = (self.weirdo.x + self.weirdo.width / 2 + self.player.x + self.player.width / 2) / 2
	end

	self.camera:lockX(x)
end

function Play:changeRoom(actionpoint)
	--fadeout
	self.transition = true
	self.player:setFrozen(true)
	Timer.tween(
		0.25,
		self.alpha,
		{255},
		'linear',
		function()
			local props = actionpoint.properties
			if props.state_name then
				Gamestate.switch(state[props.state_name])
			else
				Gamestate.switch(state.play, props.map_name, props.spawn_point)
			end
		end
	)
end

function Play:checkActions(actionpoint)
	local rectangle = {
		x = self.player.x,
		y = self.player.y,
		width = self.player.width,
		height = self.player.height
	}
	if Input:pressed('accept') then
		--Get action
		if actionpoint then
			local props = actionpoint.properties or {}

			-- Check if it's a gorram door
			if props.map_name or props.state_name then
				self:changeRoom(actionpoint)
				snd.menu.door:play()
			-- Check if it's a convo trigger
			elseif props.convo_name then
				self.conversation = Conversation(cnv[props.convo_name])
			elseif props.signal then
				Signal.emit(props.signal)
			elseif actionpoint.talkTo then
				self.conversation = actionpoint:talkTo()
			end
		end
	end
end

--[[
	Overlay for the alley scene
]]
function Play:drawAlleyOverlay()
	love.graphics.setCanvas(self.canvas)
	love.graphics.clear()
	love.graphics.setColor(10, 5, 30, 255)
	love.graphics.rectangle('fill', 0, 0, self.canvas:getWidth(), self.canvas:getHeight())
	for key, light in ipairs(self.lights) do
		local x = GAME_WIDTH * 2 + light.x - gfx.overlay.alley.map:getWidth() / 2 + 6
		local y = GAME_HEIGHT * 2 + light.y - 2
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.setBlendMode('darken', 'premultiplied')
		love.graphics.draw(gfx.overlay.alley.map, x, y)
		local alpha = 128 + 16 * math.random()
		love.graphics.setColor(alpha, alpha, alpha, 255)
		love.graphics.setBlendMode('alpha')
		love.graphics.draw(gfx.overlay.alley.main, x, y)
	end
	love.graphics.setCanvas()
end

--[[
	Overlay for the campfire scene
]]
function Play:drawPartyOutsideOverlay()
	love.graphics.setCanvas(self.canvas)
	love.graphics.clear()
	love.graphics.setColor(10, 5, 30, 255)
	love.graphics.rectangle('fill', 0, 0, self.canvas:getWidth(), self.canvas:getHeight())
	local x = GAME_WIDTH * 2
	local y = GAME_HEIGHT * 2
	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.setBlendMode('darken', 'premultiplied')
	love.graphics.draw(self.light_map, x + 456, y + 192, math.sin(love.timer.getTime() * 2) * 0.05, 1, 1, 128, 128)
	local alpha = 64 + 64 * math.random()
	love.graphics.setColor(alpha, alpha, alpha, 255)
	love.graphics.setBlendMode('alpha')
	love.graphics.draw(self.light, x + 328, y + 64)
	love.graphics.setCanvas()
end

return Play
