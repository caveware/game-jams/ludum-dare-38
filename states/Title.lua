--[[
	Title screen.
]]

local Title = {}

function Title:enter()
	self.timer = 0
	Timer.during(3, function (dt)
		self.timer = self.timer + dt / 3
	end, function ()
		Gamestate.switch(state.blank_conversation, cnv.intro, function ()
			Gamestate.switch(state.play, 'sewer_1')
		end)
	end)

	snd.menu.title:play()
end

function Title:draw()
	love.graphics.setColor(255, 255, 255, 255 * math.sin(self.timer * math.pi))
	love.graphics.draw(gfx.ui.title, 480, 272, math.random() * 0.005 - 0.01, SCALE, SCALE, 240, 136)
end

return Title