--[[
	Splash screen.
]]

local Splash = {}

--[[
	Performed when the state is entered.
]]
function Splash:enter()
	-- Splash screen timer
	self.timer = 0

	-- Timer for 1.1 seconds
	Timer.during(1.1, function (dt)
		self.timer = self.timer + dt
	end, function ()
		Gamestate.switch(state.title)
	end)

	-- Play startup sound
	snd.menu.startup:play()
end

--[[
	Performed upon state draw.
]]
function Splash:draw()
	-- Draw background
	love.graphics.setColor(51, 51, 51)
	love.graphics.rectangle('fill', 0, 0, GAME_WIDTH * SCALE, GAME_HEIGHT * SCALE)

	-- Draw logo
	local height = gfx.menu.logo:getHeight()
	local scale = GAME_WIDTH / gfx.menu.logo:getWidth() * SCALE
	love.graphics.setColor(255, 255, 255, 255 * math.sin(self.timer / 1.1 * math.pi))
	love.graphics.draw(gfx.menu.logo, 0, (GAME_HEIGHT * SCALE - height * scale) / 2, 0, scale, scale)
end

return Splash