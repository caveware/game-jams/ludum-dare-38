--[[
	Loads all game states.
]]

return {
	blank_conversation = require 'states/BlankConversation',
	cat_transition = require 'states/CatTransition',
	coffee_transition = require 'states/CoffeeTransition',
	credits = require 'states/Credits',
	play = require 'states/Play',
	splash = require 'states/Splash',
	minigame = require 'states/Minigame',
	title = require 'states/Title',
	party_transition = require 'states/PartyTransition',
}