--[[
	State for the barrista minigame.
	Basically just calls the relevant functions to
	get the ball going.
]]

local Minigame = {}

function Minigame:enter(previous_state,on_quit)
	self.camera = Camera(GAME_WIDTH / 2, GAME_HEIGHT / 2, SCALE)
	self.minigame = Barista(0, 0, on_quit)
end

function Minigame:update(dt)
	self.minigame:update(dt)
end

function Minigame:draw()
	local val = love.timer.getTime() % 4 / 4
	local w = GAME_WIDTH / 10
	local h = GAME_HEIGHT / 6

	self.camera:attach()
		love.graphics.setColor(235, 247, 222)
		love.graphics.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)

		for x = -w, GAME_WIDTH + w, w do
			for y = -h, GAME_HEIGHT + h, h do
				local dx = x + val * w
				local dy = y - val * h
				local d1 = (dx % (w * 2)) + (dy % (h * 2))
				local d2 = math.sin(d1 / (w * 2 + h * 2) * math.pi)
				love.graphics.setColor(132, 176, 125, d2 * 255)
				love.graphics.circle('fill', dx, dy, 16, 4)
			end
		end

		love.graphics.setColor(255, 255, 255)
		self.minigame:draw()
	self.camera:detach()
end

return Minigame