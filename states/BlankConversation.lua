--[[
	Blank conversation
]]

local BlankConversation = {}

function BlankConversation:enter(previous_state, conversation, after)
	self.after = after
	self.conversation = Conversation(conversation)
end

function BlankConversation:update(dt)
	self.conversation:update(dt)

	if self.conversation.complete then
		self.after()
	end
end

function BlankConversation:draw()
	self.conversation:draw()
end

return BlankConversation