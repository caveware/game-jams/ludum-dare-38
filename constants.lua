--[[
	Constants for use in game.
]]

-- Game size
GAME_HEIGHT = 272
GAME_WIDTH = 480

METRE = 48
SCALE = 2

-- Gravity functions
GRAVITY = 9.8 * METRE
TERMINAL_VELOCITY = 8