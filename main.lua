--[[
	FANCY SEEING YOU HERE

	Created by:
	> Keiran Dawson
	> Mathew Dwyer
	> Josef Frank
	> Robert Preston
]]

math.randomseed(os.time())

love.graphics.setDefaultFilter('nearest')

require "constants"

-- Require libraries
require "assets"
require "libs"

-- Require game files
require "entities"
state = require "states"
choices = {}

--[[
	Performed upon initial game load.
]]
function love.load()
	-- Global data object
	data = {
		drunkeness = 0
	}

	-- Start input manager
	Input = Baton.new(require "controls")

	-- Start up initial state
	Gamestate.registerEvents()
	-- Gamestate.switch(state.minigame)
	Gamestate.switch(state.splash)
	-- Gamestate.switch(state.play, 'party_outside', 'kitchen')
	-- Gamestate.switch(state.play, 'home')

	-- Restrict dt to 15 FPS
	local old_update = love.update
	love.update = function (dt)
		dt = math.min(1 / 15, dt)
		old_update(dt)
	end
end

--[[
	Performed upon each game update.
]]
function love.update(dt)
	-- Update Baton with joystick
	local joysticks = love.joystick.getJoysticks()
	if #joysticks > 0 then
		Input.joystick = joysticks[1]
	end

	Input:update()
	Lovebird.update()
	Timer.update(dt)
end

--[[
	Performed upon each game draw.
]]
function love.draw()

end